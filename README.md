# README #

Implementacja algorytmu klasyﬁkacji z
wykorzystaniem platformy Storm.

# Set up #
Konfiguracja topologii znajduje się w pliku ```qlearn.conf```
Znajdujące się w nim parametry:

* dimensions -- liczba wymiarów klasyfikowanego zbioru danych

* statesPerDimension -- liczba stanów (przedziałów klasyfikacji) branych pod uwagę w ramach jednego wymiaru.

* numberOfClasses -- liczba klas w klasyfikowanym zbiorze danych (tyle też klas wygeneruje ClusterDatasourceSpout).

* learningRate -- z przedziału [0, 1], współczynnik uczenia się algorytmu.

* localMode -- true dla trybu lokalnego, false dla rozproszonego.

* workers -- liczba workerów Storma.

* rewardOutputFile -- nazwa pliku z efektami weryfikacji klasyfikacji. 

* finalOutputFile -- nazwa pliku z efektami ilościowymi klasyfikacji.

## Tryb rozproszony ##

Kompilacja pliku JAR
```
mvn compile package

```
Uruchomienie na klastrze
```
./run-on-cluster
```
## Tryb lokalny ##

Uruchomienie w trybie lokalnym
```
mvn compile exec:java
```
