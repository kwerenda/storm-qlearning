#!/bin/bash

#run in local mode
mvn compile exec:java -Dstorm.topology=storm.qlearn.QlearnTopology

##submit to cluster (todo: add conf parameters)
# storm nimbus &
# storm supervisor &
# stor ui &
# mvn package
# storm jar target/storm-qlearn-0.1-jar-with-dependencies.jar storm.qlearn.example.ExampleTopology -c nimbus.host=sandbox.hortonworks.com