package storm.qlearn;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.qlearn.data.Datapoint;
import storm.qlearn.data.DatapointGenerator;

import java.util.Map;

/**
 * Creates a stream of points for classification.
 */
public class DatasourceSpout extends BaseRichSpout {
    public static Logger LOG = LoggerFactory.getLogger(DatasourceSpout.class);

    private SpoutOutputCollector collector;
    private final DatapointGenerator generator;
    private final int statesPerDimension;

    /**
     * Constructor.
     *
     * @param generator          generator that generates consecutive points
     * @param statesPerDimension number of states per point's dimension
     */
    public DatasourceSpout(DatapointGenerator generator, int statesPerDimension) {
        this.generator = generator;
        this.statesPerDimension = statesPerDimension;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream("points", new Fields("id", "values", "state"));
        declarer.declareStream("expectations", new Fields("id", "action"));
    }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void nextTuple() {
        Datapoint point = generator.nextPoint();

        collector.emit("points", new Values(point.getId(), point.getValues(), getPointGroup(point)));
        collector.emit("expectations", new Values(point.getId(), point.getClassNumber()));
    }

    /**
     * Assigns a group number to every data point. The group number is effectively the state the point belongs to.
     *
     * @param point point for which the group should be assigned
     * @return assigned group
     */
    private int getPointGroup(Datapoint point) {
        int n = point.getSize();
        double[] values = point.getValues();

        int group = 0;
        for (int i = n - 1; i > 0; --i) {
            group *= statesPerDimension;
            group += (int) (values[i] * statesPerDimension);
        }

        return group;
    }
}
