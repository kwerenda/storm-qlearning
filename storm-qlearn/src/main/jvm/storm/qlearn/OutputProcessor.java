package storm.qlearn;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.qlearn.output.FileOutput;

import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Gathers results from all agents and allows further processing.
 */
public class OutputProcessor extends BaseRichBolt {
    public static Logger LOG = LoggerFactory.getLogger(DatasourceSpout.class);
    private OutputCollector collector;

    private String classificationResultFilename;
    private FileOutput output = null;

    private long seconds = 0;
    private long count = 0;

    private Timer timer;

    /**
     * Default constructor.
     */
    public OutputProcessor() {
    }

    /**
     * Constructor. The given file path will be used to save the output from the bolt.
     * @param classificationResultFilename location of the output file
     */
    public OutputProcessor(String classificationResultFilename) {
        this.classificationResultFilename = classificationResultFilename;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;


        if (classificationResultFilename != null) {
            try {
                output = new FileOutput(classificationResultFilename);
            } catch (IOException e) {
                LOG.error("Result output file cannot be opened.");
                e.printStackTrace();
            }
            
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    output.write(String.format("%d %d\n", seconds, count));
                    output.flush();
                    ++seconds;
                }
            }, 0, 1000);
        }
    }

    @Override
    public void execute(Tuple input) {
        ++count;
        collector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    }

    @Override
    public void cleanup() {
        if (timer != null) {
            timer.cancel();
        }

        output.close();
    }

}
