package storm.qlearn;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;
import storm.qlearn.data.ClusterDatapointGenerator;
import storm.qlearn.data.DatapointGenerator;

import java.io.FileInputStream;
import java.util.Properties;

/**
 * Defines the topology for the Q-learning implementation.
 */
public class QlearnTopology {

    private final static String stdConfigFilePath = "qlearn.conf";

    public static void main(String[] args) throws Exception {

        String configFilePath = args.length > 1 ? args[1] : stdConfigFilePath;
        FileInputStream fis = new FileInputStream(configFilePath);
        Properties properties = new Properties();
        properties.load(fis);

        int dimensions = Integer.parseInt(properties.getProperty("dimensions"));
        double learningRate = Double.parseDouble(properties.getProperty("learningRate"));
        int numberOfClasses = Integer.parseInt(properties.getProperty("numberOfClasses"));
        int statesPerDimension = Integer.parseInt(properties.getProperty("statesPerDimension"));
        boolean localMode = Boolean.parseBoolean(properties.getProperty("localMode"));
        int workers = Integer.parseInt(properties.getProperty("workers"));

        String rewardOutputFile = properties.getProperty("rewardOutputFile");
        String finalOutputFile = properties.getProperty("finalOutputFile");

        TopologyBuilder builder = new TopologyBuilder();

        DatapointGenerator generator = new ClusterDatapointGenerator(dimensions, numberOfClasses);

        builder.setSpout("data-source", new DatasourceSpout(generator, statesPerDimension));

        // create a proper number of tasks
        int tasks = dimensions * statesPerDimension;
        builder.setBolt("agent", new QlearningAgent(numberOfClasses, learningRate), tasks)
                .fieldsGrouping("data-source", "points", new Fields("state"))
                .directGrouping("reward", "rewards");

        builder.setBolt("reward", new RewardingBolt(rewardOutputFile), 1)
                .globalGrouping("agent")
                .globalGrouping("data-source", "expectations");
        builder.setBolt("output", new OutputProcessor(finalOutputFile))
                .shuffleGrouping("agent");

        Config config = new Config();
        config.setDebug(false);

        if (localMode) {
            LocalCluster cluster = new LocalCluster();
            cluster.submitTopology("qlearn", config, builder.createTopology());
            Utils.sleep(20000);
            cluster.killTopology("qlearn");
            cluster.shutdown();
        } else {
            config.setNumWorkers(workers);
            StormSubmitter.submitTopology(args[0], config, builder.createTopology());
        }

    }
}
