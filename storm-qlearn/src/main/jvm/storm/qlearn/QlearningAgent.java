package storm.qlearn;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;

/**
 * Single agent for data processing. Contains a fragment of the Q table for any
 * number of states. Responsible for assigning actions to points.
 */
public class QlearningAgent extends BaseRichBolt {
    private OutputCollector collector;
    private final int numberOfActions;

    private final double learningRate;

    private final Map<Integer, double[]> qTable = new HashMap<Integer, double[]>();

    /**
     * Constructor.
     *
     * @param numberOfClasses number of available classes to choose from
     * @param learningRate    learning rate for adjusting the Q table with rewards
     */
    public QlearningAgent(int numberOfClasses, double learningRate) {
        this.numberOfActions = numberOfClasses;
        this.learningRate = learningRate;
    }


    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(Tuple input) {
        if ("reward".equals(input.getSourceComponent())) {

            int state = input.getIntegerByField("state");
            int action = input.getIntegerByField("action");
            int id = input.getIntegerByField("id");
            double reward = input.getDoubleByField("reward");

            adjustQTable(state, action, reward);
        } else {

            int state = input.getIntegerByField("state");
            int action = getProbableAction(state);

            Values values = new Values(input.getIntegerByField("id"), action, state);

            collector.emit(input, values);
        }
        collector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("id", "action", "state"));
    }

    /**
     * Selects the best (highest-valued) action for a given state.
     *
     * @param state
     * @return the best action
     */
    private int getProbableAction(int state) {
        if (!qTable.containsKey(state)) {
            double[] actions = new double[numberOfActions];
            for (int i = 0; i < actions.length; ++i) {
                actions[i] = 1;
            }

            qTable.put(state, actions);
        }

        double[] actions = qTable.get(state);
        int action = 0;
        double maxValue = 0;
        for (int i = 0; i < actions.length; ++i) {
            if (actions[i] > maxValue) {
                maxValue = actions[i];
                action = i;
            }
        }

        return action;
    }

    /**
     * Adjusts value in the Q table for a given state, action and reward.
     *
     * @param state  state the point is in
     * @param action action selected for the point
     * @param reward reward for the action
     */
    private void adjustQTable(int state, int action, double reward) {
        if (!qTable.containsKey(state)) {
            return;
        }

        double[] actions = qTable.get(state);
        actions[action] = actions[action] + learningRate * (reward - actions[action]);
    }
}
