package storm.qlearn;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import storm.qlearn.output.FileOutput;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Calculates and sends rewards to agents.
 */
public class RewardingBolt extends BaseRichBolt {
    private OutputCollector collector;
    public static Logger LOG = LoggerFactory.getLogger(DatasourceSpout.class);

    // stores expected actions
    private final Map<Integer, Integer> expected = new HashMap<Integer, Integer>();

    // stores received actions
    private final Map<Integer, Result> received = new HashMap<Integer, Result>();

    private int result = 0;
    private int n = 0;

    private final String outputFile;
    private FileOutput output;

    public RewardingBolt(String outputFile) {
        this.outputFile = outputFile;
    }

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;

        try {
            this.output = new FileOutput(outputFile);
        } catch (IOException e) {
            System.out.println("Output to file is not available.");
            e.printStackTrace();
        }
    }

    @Override
    public void execute(Tuple input) {
        int id = input.getIntegerByField("id");
        int action = input.getIntegerByField("action");

        if ("expectations".equals(input.getSourceStreamId())) {
            if (!expected.containsKey(id)) {
                expected.put(id, action);
            }
        } else {
            if (!received.containsKey(id)) {
                int state = input.getIntegerByField("state");
                int taskid = input.getSourceTask();
                received.put(id, new Result(action, taskid, state));
            }
        }

        if (received.containsKey(id) && expected.containsKey(id)) {
            double reward = getReward(id);

            Result result = received.get(id);
            collector.emitDirect(result.getTaskid(), "rewards", new Values(id, result.getState(), action, reward));
            received.remove(id);
            expected.remove(id);

            collector.emit("results", new Values(result, n));
        }

        collector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declareStream("rewards", true, new Fields("id", "state", "action", "reward"));
        declarer.declareStream("results", new Fields("correct", "total"));
    }

    /**
     * Calculates the reward for a particular point id.
     *
     * @param id point's id
     * @return value of the reward
     */
    private double getReward(int id) {
        this.n++;

        int receivedAction = received.get(id).getAction();
        if (expected.get(id) == receivedAction) {
            this.result++;
            if (n % 10000 == 0) {
                output.write(String.format("%d %d %d\n", id, result, n));
                output.flush();
            }

            return 1.0;
        } else {
            return 0.0;
        }
    }

    @Override
    public void cleanup() {
        output.close();
    }

    /**
     * Stores information about one received classification result.
     */
    private static class Result {
        private final int action;
        private final int taskid;
        private final int state;

        public Result(int action, int taskid, int state) {
            this.action = action;
            this.taskid = taskid;
            this.state = state;
        }

        /**
         * Returns the action selected for the point.
         */
        public int getAction() {
            return action;
        }

        /**
         * Returns the task id of the agent that sent the result.
         */
        public int getTaskid() {
            return taskid;
        }

        /**
         * Returns the state to which the point belongs.
         */
        public int getState() {
            return state;
        }
    }
}
