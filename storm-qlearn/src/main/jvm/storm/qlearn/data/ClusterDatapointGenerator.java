package storm.qlearn.data;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Generates data points divided into clusters.
 */
public class ClusterDatapointGenerator implements DatapointGenerator, Serializable {

    private final int dimensions;
    private static final int MAX_WEIGHT = 10;
    private static final double MIN_STD = 0.001;
    private static final double MAX_STD = 0.050;
    private final double[][] centers;
    private final List<Integer> classes = new ArrayList<Integer>();
    private final double[] stds;
    private final int nClasses;
    private final Random random = new Random(0); //same seed everywhe
    private int id = 1;


    private int drawClass() { // weighted for variable cluster size
        return classes.get(this.random.nextInt(classes.size()));
    }


    /**
     * Constructor.
     *
     * @param dimensions number of dimensions (features) for each point
     * @param nClasses   number of classes to generate
     */
    public ClusterDatapointGenerator(int dimensions, int nClasses) {
        this.dimensions = dimensions;
        this.nClasses = nClasses;
        this.centers = new double[this.nClasses][this.dimensions];
        this.stds = new double[this.nClasses];


        //weight from 1 to 10, std from 0.001 to 0.050
        for (int i = 0; i < nClasses; i++) {
            int w = this.random.nextInt(MAX_WEIGHT) + 1;
            stds[i] = MIN_STD + this.random.nextDouble() * (MAX_STD - MIN_STD);
            for (int k = 0; k < w; k++) {
                classes.add(i);
            }
        }

        // generate class centers
        for (int c = 0; c < nClasses; c++) {
            for (int d = 0; d < this.dimensions; d++) {
                centers[c][d] = this.random.nextDouble();
            }
        }

        //testowac dla 1d
    }

    @Override
    public Datapoint nextPoint() {

        int c = drawClass();

        //generate point from this class (classes can happen to overlap)
        double[] values = new double[this.dimensions];
        for (int d = 0; d < this.dimensions; d++) {
            values[d] = -1.0;
            while (values[d] < 0.0 || values[d] > 1.0) { //in [0,1]
                values[d] = this.random.nextGaussian() * stds[c] + this.centers[c][d];
            }
        }
        return new Datapoint(this.id++, values, c);
    }
}
