package storm.qlearn.data;

import java.util.Arrays;

/**
 * Represents a single multidimensional data point.
 */
public class Datapoint {
    private int id;
    private double[] values;
    private int classNumber = 0;

    /**
     * Constructor.
     *
     * @param id     the point's id
     * @param values point's features
     */
    public Datapoint(int id, double[] values) {
        this.id = id;
        this.values = values;
    }

    /**
     * Constructor. Allows to additionally store point's class.
     *
     * @param id          the point's id
     * @param values      point's features
     * @param classNumber point's class
     */
    public Datapoint(int id, double[] values, int classNumber) {
        this(id, values);
        this.classNumber = classNumber;
    }

    /**
     * Returns the point's id.
     */
    public int getId() {
        return id;
    }

    /**
     * Returns an array of point's features.
     */
    public double[] getValues() {
        return values;
    }

    /**
     * Returns the number of features.
     */
    public int getSize() {
        return values.length;
    }

    /**
     * Returns point's class number.
     */
    public int getClassNumber() {
        return classNumber;
    }

    @Override
    public String toString() {
        return "Datapoint{" +
                "id=" + id +
                ", values=" + Arrays.toString(values) +
                ", classNumber=" + classNumber +
                '}';
    }
}
