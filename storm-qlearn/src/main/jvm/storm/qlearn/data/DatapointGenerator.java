package storm.qlearn.data;

/**
 * Common interface for generators of data points.
 */
public interface DatapointGenerator {

    /**
     * Returns the next point.
     *
     * @return data point
     */
    Datapoint nextPoint();
}
