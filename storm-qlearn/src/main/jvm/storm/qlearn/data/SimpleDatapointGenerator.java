package storm.qlearn.data;

import java.io.Serializable;
import java.util.Random;

/**
 * Generates random points.
 */
public class SimpleDatapointGenerator implements DatapointGenerator, Serializable {
    private final Random random = new Random();
    private int id = 1;
    private final int dimensions;

    public SimpleDatapointGenerator(int dimensions) {
        this.dimensions = dimensions;
    }

    public Datapoint nextPoint() {
        double[] values = new double[dimensions];
        for (int i = 0; i < dimensions; ++i) {
            values[i] = random.nextDouble();
        }

        return new Datapoint(id++, values);
    }
}
