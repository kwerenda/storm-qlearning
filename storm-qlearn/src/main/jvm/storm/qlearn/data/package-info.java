/**
 * Data point generators and classes related in any way to input data.
 */
package storm.qlearn.data;