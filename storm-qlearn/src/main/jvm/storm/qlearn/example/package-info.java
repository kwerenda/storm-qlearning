/**
 * Contains slightly modified examples from the Storm's documentation.
 * Everything here is used just for testing purposes.
 */
package storm.qlearn.example;