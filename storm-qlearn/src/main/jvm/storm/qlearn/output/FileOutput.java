package storm.qlearn.output;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Simple wrapper around output to file operations, provided to some bolts for simplicity.
 */
public class FileOutput implements Serializable {

    private final BufferedWriter writer;

    /**
     * Constructor.
     *
     * @param filepath path to the output file
     * @throws IOException
     */
    public FileOutput(String filepath) throws IOException {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String dateStr = dateFormat.format(cal.getTime());

        Path path = Paths.get(filepath + "." + dateStr);
        writer = Files.newBufferedWriter(path, Charset.defaultCharset());
    }

    /**
     * Writes the given string to the file.
     *
     * @param text text to write
     */
    public void write(String text) {
        try {
            writer.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Flushes the buffer.
     */
    public void flush() {
        try {
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Closes the file.
     */
    public void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
