package storm.qlearn.reward;

/**
 * Calculates a reward for a given action and state.
 */
@Deprecated
public class SimpleRewardCalculator {

    private double division;

    public SimpleRewardCalculator(double division) {
        this.division = division;
    }

    public double reward(double x, int action) {
        if ((x < division && action == 1) || (x > division && action == 2)) {
            return 1;
        } else {
            return 0;
        }
    }
}